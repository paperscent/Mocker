# Mocker

[Mockito](https://site.mockito.org/)-like mocking for the course "Advanced Java Programming" at OTH Regensburg.

Note that the program only works for single-threaded programs.

For detailed requirements look at the [Problem set](Problem_Set.pdf).