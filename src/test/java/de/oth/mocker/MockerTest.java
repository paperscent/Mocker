package de.oth.mocker;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static de.oth.mocker.Mocker.*;

class MockerTest {

    @Test
    void mockTest() {
        List<String> mockObject = mock(ArrayList.class);

        mockObject.add("John Doe");
        mockObject.add("Max Muster");
        mockObject.add("John Doe");
        mockObject.size();
        mockObject.clear();

        verify(mockObject, times(2)).add("John Doe");
        verify(mockObject).add("Max Muster");
        verify(mockObject, times(1)).clear();
        verify(mockObject, never()).get(5);
        verify(mockObject, atLeast(2)).add("John Doe");
        verify(mockObject, atMost(1)).add("Max Muster");
    }

    @Test
    void spyTest() {
        List<String> names = new ArrayList<>();
        List<String> spyList = spy(names);

        spyList.add("John Doe");
        spyList.add("Max Muster");
        spyList.add("John Doe");
        spyList.add("Max");

        verify(spyList, atMost(2)).add("John Doe");
        verify(spyList, atLeast(1)).add("Max Muster");
        verify(spyList, never()).clear();
        verify(spyList, times(1)).add("Max");

        try {
            verify(spyList, times(3)).add("John Doe");
            throw new Error("AssertionError should have been thrown.");
        } catch (AssertionError e) { }
    }

    @Test
    void oneMoreTest() {
        Map<String, Integer> someMap = mock(HashMap.class);
        Map<Object, Integer> otherMap = mock(HashMap.class);

        someMap.put("Hello", 3);
        someMap.put("Hello", 3);

        otherMap.put(someMap, 1);
        otherMap.containsKey(new HashMap<>());

        verify(someMap, times(2)).put("Hello", 3);

        try {
            verify(someMap, times(3)).put("Hello", 3);
            throw new Error("AssertionError should have been thrown.");
        } catch (AssertionError e) { }

        verify(otherMap, times(1)).containsKey(new HashMap<>());

        try {
            verify(otherMap, times(2)).containsKey(new HashMap<>());
            throw new Error("AssertionError should have been thrown.");
        } catch (AssertionError e) { }
    }

}