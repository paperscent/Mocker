package de.oth.mocker;

import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class SpyingIntercept<T> extends MockingIntercept {

    private T originalObject;

    public SpyingIntercept(T object) {
        originalObject = object;
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        CalledMethod calledMethod = new CalledMethod(method, objects);

        if (Mocker.verificationMode) {
            verify(calledMethod, Mocker.verificationOption.getType());
        } else {
            addToMap(calledMethod);
            return methodProxy.invoke(originalObject, objects);
        }

        Mocker.verificationMode = false;

        return null;
    }
}
