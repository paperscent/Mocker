package de.oth.mocker;

import net.sf.cglib.proxy.*;

public class Mocker {

    static boolean verificationMode = false;
    static VerifyOption verificationOption;

    public static <T> T spy(T objectToMock) {

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(objectToMock.getClass());
        enhancer.setCallback(new SpyingIntercept(objectToMock));

        return (T) enhancer.create();
    }

    public static <T> T mock(Class<T> classToMock) {

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(classToMock);
        enhancer.setCallback(new MockingIntercept());

        return (T) enhancer.create();
    }

    public static <T> T verify(T mockedObject, VerifyOption option) {

        verificationMode = true;
        verificationOption = option;

        return mockedObject;
    }

    public static <T> T verify(T mockedObject) {
        return verify(mockedObject, VerifyOption.NewOption(VerifyOption.VerifyType.times, 1));
    }

    public static VerifyOption times(int i) {
        if (i < 1) {
            throw new Error("You have to choose a number >= 1 for the 'times' option. Alternatively, use 'never' if " +
                    "you want to check for the number '0'.");
        }

        return VerifyOption.NewOption(VerifyOption.VerifyType.times, i);
    }

    public static VerifyOption never() {

        return VerifyOption.NewOption(VerifyOption.VerifyType.never);
    }

    public static VerifyOption atLeast(int i) {
        if (i < 1) {
            throw new Error("You have to choose a number >= 1 for the 'atLeast' option.");
        }

        return VerifyOption.NewOption(VerifyOption.VerifyType.atLeast, i);
    }

    public static VerifyOption atMost(int i) {
        if (i < 1) {
            throw new Error("You have to choose a number >= 1 for the 'atMost' option.");
        }

        return VerifyOption.NewOption(VerifyOption.VerifyType.atMost, i);
    }
}
