package de.oth.mocker;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.HashMap;

public class MockingIntercept implements MethodInterceptor {

    private HashMap<CalledMethod, Integer> callHistory = new HashMap<>();

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        CalledMethod calledMethod = new CalledMethod(method, objects);

        if (Mocker.verificationMode) {
            verify(calledMethod, Mocker.verificationOption.getType());
        } else {
            addToMap(calledMethod);
        }

        Mocker.verificationMode = false;

        return null;
    }

    void verify(CalledMethod method, VerifyOption.VerifyType type) {

        // If user calls "verify(mockObject, times(2)).add("John Doe");" -> numberToCheck = 2
        int numberToCheck = Mocker.verificationOption.getNumber();

        // How often the method was actually called
        int actualNumber = callHistory.getOrDefault(method, 0);

        if (type == VerifyOption.VerifyType.times) {
            if (numberToCheck != actualNumber) {
                throw new AssertionError("Checked for times. Expected: " + numberToCheck + " Actual: " + actualNumber);
            }

        } else if (type == VerifyOption.VerifyType.atLeast) {
            if (actualNumber - numberToCheck < 0) {
                throw new AssertionError("Checked for atLeast. Expected: " + numberToCheck + " Actual: " + actualNumber);
            }

        } else if (type == VerifyOption.VerifyType.atMost) {
            if (actualNumber > numberToCheck) {
                throw new AssertionError("Checked for atMost. Expected: " + numberToCheck + " Actual: " + actualNumber);
            }

        } else if (type == VerifyOption.VerifyType.never) {
            if (actualNumber != 0) {
                throw new AssertionError("Checked for never. But method was called.");
            }
        }
    }

    /**
     * Add key to the callHistory. In case the key is already in the callHistory, increase the value by 1.
     */
    void addToMap(CalledMethod method) {
        if (!callHistory.containsKey(method)) {
            callHistory.put(method, 1);
        } else {
            int value = callHistory.get(method);
            callHistory.put(method, value + 1);
        }
    }
}