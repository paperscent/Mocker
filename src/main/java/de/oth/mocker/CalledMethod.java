package de.oth.mocker;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;

/**
 * Just a class to store the called methods. Used in the HashMap (see 'MockingIntercept.java'). The equals() and
 * hashCode() methods are needed because objects of this type are stored in the HashMap and we use the containsKey()
 * method in 'MockingIntercept.java' which needs the equals() method.
 */
public class CalledMethod {

    private Method method;
    private Object[] parameters;

    public CalledMethod(Method method, Object[] parameters) {
        this.method = method;
        this.parameters = parameters;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalledMethod that = (CalledMethod) o;
        return Objects.equals(method, that.method) &&
                Arrays.equals(parameters, that.parameters);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(method);
        result = 31 * result + Arrays.hashCode(parameters);
        return result;
    }
}
