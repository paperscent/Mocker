package de.oth.mocker;

public class VerifyOption {

    enum VerifyType {times, never, atLeast, atMost}

    private VerifyType type;
    private int i = -1;

    static VerifyOption NewOption(VerifyType type, int i) {
        return new VerifyOption(type, i);
    }

    static VerifyOption NewOption(VerifyType type) {
        return new VerifyOption(type);
    }

    private VerifyOption(VerifyType type, int i) {
        this.type = type;
        this.i = i;
    }

    private VerifyOption(VerifyType type) {
        this.type = type;
    }

    VerifyType getType() {
        return type;
    }

    int getNumber() {
        return i;
    }
}
